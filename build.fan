using build
using stackhub

class Build : BuildPod {

	new make() {
		podName = "particleExt"
		summary = "Particle Connector Extension"
		version = Version([1,0,3])

		meta = [
      "org.name": "Jay Herron",
			"proj.name" : "particleExt",
      "skyspark.docExt": "true"
		]

		depends = [
      "axon 3.0",
      "connExt 3.0.22+", // Needed for Conn 3.0 interface
      "folio 3.0",
      "fresco 3.0",
      "haystack 3.0",
      "hisExt 3.0",
      "skyarcd 3.0",
			"sys 1.0",
      "util 1.0",
      "web 1.0"
		]

		srcDirs = [`fan/`]
		resDirs = [`lib/`]
    
    index = [
      "skyarc.ext": "particleExt::ParticleExt",
      "skyarc.lib": "particleExt::ParticleLib"
    ]

		docApi = true
		docSrc = true
	}
  
  ** Publishes to StackHub. To run, open a console and run:
  ** 
  ** fan /path/to/this/pod/build.fan publish
  @Target { help = "Publish to stackhub.org" }
  Void publish() {
    stackhub::PublishTask(this).run
  }
}
