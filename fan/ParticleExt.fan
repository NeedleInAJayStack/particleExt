using connExt
using skyarcd

** A simple implmentation of the ConnImplExt class
@ExtMeta
{
  name = "particle"
  icon = "cpu"
  depends = ["conn"]
}
const class ParticleExt : ConnImplExt{
  @NoDoc new make() : super(ParticleModel()) {}
  
  override Void onStart() {
    super.onStart()
  }
}
