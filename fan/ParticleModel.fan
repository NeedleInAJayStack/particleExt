using connExt
using haystack

** This defines the Particle Connector Model
@Js
const class ParticleModel : ConnModel{
  new make() : super(ParticleModel#.pod, "particle") {
    connTag = "particleConn"
    connRefTag = "particleConnRef"
    connProto = Etc.makeDict([
      "dis": "Particle Connector",
      "username": "",
      "password": ""
    ])
    pointCurTag = "particleCur"
    learnFunc = "particleLearn"
    pingFunc = "particlePing"
  }
  override const Dict connProto
  override Bool isCurSupported() { true }
  override Bool isLearnSupported() { true }
  override PollingMode pollingMode() { PollingMode.buckets }
}
