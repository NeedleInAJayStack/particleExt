using connExt
using util
using web


** This contains methods that handle formatting queries and parsing responses from the Particle API
class ParticleApi {
  ParticleConn conn
  
  new make(ParticleConn conn){
    this.conn = conn
  }
  
  ** Returns an access token object for the particle connector
  public [Str:Obj] getAccessToken(Duration dur){
    InStream res := WebClient(`https://api.particle.io/oauth/token`).postForm([
      "grant_type": "client_credentials",
      "client_id": conn.rec["username"],
      "client_secret": conn.proj.passwords.get(conn.id.toStr),
      "expires_in": dur.toSec().toStr()
    ]).resIn()
    
    return parseResponse(res)
  }
  
  ** Performs the 'devices' request documented [here]`https://docs.particle.io/reference/device-cloud/api/#list-devices`.
  ** Please see that documentation for information on parameters and returns.
  public [Str:Obj?][] getDevices([Str:Str] params := [:]){
    WebClient client := query("/v1/devices/", params)
    return parseResponse(client.getBuf().in())
  }
  
  ** Performs the 'device' request documented [here]`https://docs.particle.io/reference/device-cloud/api/#get-device-information`
  ** Please see that documentation for information on parameters and returns.
  public [Str:Obj?] getDeviceInfo(Str deviceId, [Str:Str] params := [:]){
    WebClient client := query("/v1/devices/"+deviceId, params)
    return parseResponse(client.getBuf().in())
  }
  
  ** Performs the 'variable' request documented [here]`https://docs.particle.io/reference/device-cloud/api/#get-a-variable-value`
  ** Please see that documentation for information on parameters and returns.
  public [Str:Obj?] getVariable(Str deviceId, Str varName, [Str:Str] params := [:]){
    WebClient client := query("/v1/devices/"+deviceId+"/"+varName+"/", params)
    return parseResponse(client.getBuf().in())
  }
  
  ** Builds a WebClient to perform the query to the given endpoint with the provided parameters
  ** 
  ** **Params**
  ** 
  ** - 'endPoint': The API endpoint, as identified in the Particle API docs. Must start with "/"
  ** - 'params': Map of the parameters to include. 'access_token' is added automatically.
  private WebClient query(Str endPoint, [Str:Str] params := [:]){
    if(!endPoint.startsWith("/")) throw ArgErr("endPoint must start with a slash: "+endPoint)
    
    Str uriStr := "https://api.particle.io"+endPoint
    
    params.set("access_token", conn.getAccessToken()) // Set access token
    uriStr = uriStr+"?" // Get ready to add parameters
    Bool firstRow := true
    params.each(|Str val, Str key|{
      if(firstRow) {
        uriStr = uriStr+key+"="+val
        firstRow = false
      }
      else uriStr = uriStr+"&"+key+"="+val // Add & before key=val for all but the first entry
    })
    Uri uri := Uri.fromStr(uriStr)
    
    return WebClient(uri)
  }
  
  ** Parses a Particle JSON response instream into an object, and throws an error if
  ** one occurred during the request.
  private Obj parseResponse(InStream res){
    Obj json := JsonInStream(res).readJson()
    
    // If the result is a map that contains an error key, the query was unsuccessful
    if(json is Map && ((Map)json).containsKey("error")) {
      Map jsonMap := json
      throw Err(jsonMap["error"].toStr()+": "+jsonMap["info"])
    }
    
    return json
  }
}
