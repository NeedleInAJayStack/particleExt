using axon
using connExt
using haystack
using skyarcd

** These are the Axon-accessible methods
const class ParticleLib {
  
  ** Sync cur values from the particle cloud.
  ** 
  ** **Params**
  ** 
  ** - 'points': The points that should have their cur synced
  @Axon
  public static Obj? particleSyncCur(Obj points){
    return getExt().syncCur(points)
  }
  
  ** Pings the particle connector.
  ** 
  ** **Params**
  ** 
  ** - 'conn': The connector to ping.
  @Axon
  public static Dict? particlePing(Obj conn){
    return getConn(conn).onPing()
  }
  
  ** Learns the Particle tree, traversing devices and variables.
  ** 
  ** **Params**
  ** 
  ** - 'args': A Str List whose number of contents determine the level of learn to perform. The 
  ** following are possible:
  **     - '[]': Learn devices
  **     - '[{deviceId}]': Learn device variables
  ** 
  ** **Return**
  ** 
  ** The objects available at the specified level of the learn.
  @Axon
  public static Grid particleLearn(Obj conn, Obj? args){
    return getConn(conn).onLearn(args)
  }
  
  ** Returns the Particle extension
  private static ParticleExt getExt(){
    return Context.cur().ext("particle")
  }
  
  ** Returns an instance of the ParticleConn class, as defined by the provided
  ** connector dict
  private static ParticleConn getConn(Obj conn){
    Dict connRec := SysLib.toRec(conn)
    ConnActor connActor := ((ConnImplExt) getExt()).connActor(connRec)
    return ParticleConn(connActor, connRec)
  }
}
