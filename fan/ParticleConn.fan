using connExt
using folio
using haystack
using hisExt

** The Particle Conn implementation. For overridden functions, please see Conn documentation.
class ParticleConn : Conn {
  
  ** Denotes the delimiter used in the particleCur tag
  Int delim := '/'
  
  new make(ConnActor actor, Dict rec) : super(actor, rec) {}
  
  override Void onOpen(){
    this.onPing()
  }
  
  override Void onClose() {}
  
  override Dict onPing() {
    ParticleApi(this).getDevices() // Just make sure a query can run successfully
    return Etc.makeDict([:])
  }
  
  override Void onSyncCur(ConnPoint[] points) {
    points.each(|ConnPoint point|{
      try {
        Str? particleCur := point.rec["particleCur"]
        if(particleCur == null) throw Err("particleCur tag is not present: "+point.id().toStr())
        Str deviceId := particleCur.split('/')[0]
        Str varName := particleCur.split('/')[1]
        [Str:Obj?] varObj := ParticleApi(this).getVariable(deviceId, varName)
        Obj? val := varObj["result"]
        
        // Parse the val to Haystack types correctly. Particle only supports double, int, and string
        if(val is Float) point.updateCurOk(Number.make(val))
        else if(val is Int) point.updateCurOk(Number.makeInt(val))
        else if(val is Str) point.updateCurOk(val)
      }
      catch (Err err) {
        point.updateCurErr(err)
      }
    })
  }
  
  ** Implements learn functionality.
  ** 
  ** **Param**
  ** 
  ** - 'args': A Str list whose number of contents determine the level of learn to perform. The 
  ** following are possible:
  **     - '[]': Learn devices
  **     - '[{deviceId}]': Learn device variables
  ** 
  ** **Return**
  ** 
  ** The objects available at the provided learn level.
  override Grid onLearn(Obj? arg){
    if(arg == null) return learnDevices()
    else {
      Int size := ((Str[])arg).size
      if(size == 0) return learnDevices()
      else return learnVariables(arg)
    }
  }
  
  ** Refreshes the current OAuth access token and updates the oauthExpiresAt tag on the connector.
  ** 
  ** **Params**
  ** 
  ** - 'dur': The duration of access token to request.
  public Void refreshAccessToken(Duration dur := Duration.fromStr("1day")){
    ParticleApi api := ParticleApi(this)
    [Str:Obj] token := api.getAccessToken(dur)
    
    // Store the token itself in the password store
    proj.passwords.set(tokenKey(), token["access_token"])
    
    // Store the expiry time in a tag on the connector record
    DateTime expiresAt := DateTime.now() + Duration.fromStr(token["expires_in"].toStr()+"sec")
    ext.proj.commit(Diff(rec, Etc.makeDict(["oauthExpiresAt": expiresAt])))
  }
  
  ** Returns a valid OAuth access token Str. If the current token has expired, the system will 
  ** request a new token and return its value. 
  protected Str getAccessToken(){
    // Refresh token if it has expired already
    if(!rec.has("oauthExpiresAt") || DateTime.now() > rec["oauthExpiresAt"]) {
      refreshAccessToken()
    }
    return proj.passwords.get(tokenKey())
  }
  
  ** Returns the key used for the OAuth key in the password store.
  private Str tokenKey(){
    return id.toStr()+"_accessToken"
  }
  
  ** Returns a list of the devices available to the Particle connector, along with information 
  ** on each device. This is a learn helper function run at the first learn level.
  private Grid learnDevices(){
    ParticleApi api := ParticleApi(this)
    [Str:Obj?][] devices := api.getDevices()
    
    [Str:Obj?][] result := [,]
    devices.each(|[Str:Obj?] device|{
      Str deviceId := device["id"]
      Str[] newLearn := [deviceId]
      [Str:Obj] resultRow := [
        "dis": device["name"],
        "status": device["status"],
        "connected": device["connected"],
        "lastHeard": DateTime.fromIso(device["last_heard"]),
        "cellular": device["cellular"],
        "notes": device["notes"],
        "learn": newLearn
      ]
      result.add(resultRow)
    })
    return Etc.makeMapsGrid(null, result)
  }
  
  ** Returns a list of the variables on the device, as well as any point tagging that can be 
  ** inferred from the Particle API. This is a learn helper function run at the second learn level.
  ** 
  ** **Param**
  ** 
  ** - 'learnPath': Array conatinaining only the device id on which to search for variables. 
  private Grid learnVariables(Str[] learnPath){
    Str deviceId := learnPath[0] 
    
    ParticleApi api := ParticleApi(this)
    [Str:Obj?] deviceInfo := api.getDeviceInfo(deviceId)
    [Str:Obj?]? variables := deviceInfo["variables"]
    if(variables == null) return Etc.makeEmptyGrid()
    
    [Str:Obj?][] result := [,]
    variables.each(|Obj? val, Str name|{
      Str[] newLearn := learnPath.dup().add(name) 
      
      [Str:Obj?] point := [
        "dis": name.capitalize(),
        "particleCur": makePath(newLearn),
        "point": Marker.val,
        "sensor": Marker.val
      ]
      if(val == "int32" || val == "double") point.set("kind", "Number")
      else if(val == "string") point.set("kind", "Str")
      
      result.add(point)
    })
    return Etc.makeMapsGrid(null, result)
  }
  
  ** Creates a particleCur path from the given path components (in order) and the delimiter.
  ** 
  ** **Param**
  ** 
  ** - 'components': The components of the path. Typically '[deviceId, varName]'
  private Str makePath(Str[] components){
    Str delimStr := Str.fromChars([delim])
    return components.join(delimStr)
  }
}
