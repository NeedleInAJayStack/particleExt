# Particle Connector Extension #

This is a SkySpark extension that allows SkySpark to communicate with the [Particle Device Cloud](https://www.particle.io/device-cloud/).

Please see the pod.fandoc file for documentation, setup instructions, and more.

## Contributing ##

Anyone is welcome to contribute!

## License ##

This code is licensed under the MIT License

## ChangeLog ##

### v0.1.2 ###
* Connector learn support

### v0.1.1 ###
* StackHub Integration

### v0.1.0 ###
* Initial Release